import sys
import os
import traceback
import logging

import zoro_messaging_queue.zmq as zmq
import zoro_logging.logstash
import config

logger = logging.getLogger("outbound_etl_utilities")
logstash_handler = zoro_logging.logstash.get_handler(
                        host=config.logging["host"],
                        port=config.logging["port"])
logger.addHandler(logstash_handler)
logger.setLevel(config.logging["level"])

zmq_setting = {
    'userid': config.rsxml_publisher["username"],
    'password': config.rsxml_publisher["password"],
    'hostname': config.rsxml_publisher["host"],
    'port': config.rsxml_publisher["port"],
    'exchange_name': config.rsxml_publisher["publish_exchange"],
    'exchange_type': config.rsxml_publisher["publish_exchange_type"],
    'routing_key': config.rsxml_publisher["routing_key"]
}

filename = sys.argv[1]

with open(filename) as f:
    try:
        zmq.publish_message(f.read(), **zmq_setting)
        logger.debug("Successfully published rsxml message")
    except Exception as e:
        log_obj = {
            "error": "Failed to publish rsxml message",
            "exception": e,
            "traceback": traceback.format_exc()
        }
        logger.error(log_obj)
