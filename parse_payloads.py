import sys
import json

with open(sys.argv[1], "r") as payloads:
    objs = payloads.read()
    objs = json.loads(objs)

for obj in objs["hits"]["hits"]:
    payload_list = json.loads(obj["_source"]["payload"])
    for payload in payload_list:
        zoroNo = payload.get("zoroNo")
        file_name = "./payloads/{}".format(zoroNo)
        with open(file_name, "w") as write_file:
            write_file.write(json.dumps(payload))
